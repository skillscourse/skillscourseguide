# Welcome to CS266: Computing Skills!

In this course you will learn the basic technical skills that have many applications across the CS curriculum and in academic/professional spaces that depend on technical computing such as bioinformatics and data science. The course is designed to guide your development by providing resources and structured deliverables combined with tailored support and feedback. We believe that the best way to learn how to paint is to practice painting.

This is a module-based course with an emphasis on independent work. We believe that the way to learn how to paint is to practice painting (with watercolors). It consists of a base unit, composed of 7 sub-units, and one topical unit. The successor course, CS267, is composed of two topical units. Fluency in each of these skills will take longer than one semester. However, by completing these units you will have the proficiency to use the relevant skills to complete more advanced projects leading to fluency. 

Each unit builds on earlier ones, using tools and techniques learned earlier, and each with one or more deliverables to demonstrate your proficiency. In order for me to keep-up with how each of you are progressing there are two required weekly check-ins, one short meeting (either in-person or via internet telephony) and one Moodle-based quiz. 

## [Base Unit - CS Fundamentals](cs-fundamentals)

All students must complete the fundamental skills base unit. Its component skills have broad application across a wide range computer science. Each component is likely to remain relevant to anyone working in a computer science-related field for the foreseeable future.

See this [README](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/tree/master/cs-fundamentals) for a complete description of the base unit.

## Topical Units

Each student must complete one topical unit as part of enrolling in CS266. Click on the README link for more information about a particular one. If you have taken a particular topical unit before and wish to take it again, then you should use the time to push yourself further with that tool, rather than check the box and consider it done.

### [Basic skills for data analysis and visualization](topical-units/data-analysis-and-viz)
* Python, R intro
* Libraries in Python: matplotlib, seaborn
* Visualizations plus text

### [System administration](topical-units/sysadmin)
* Linux servers
* Virtualization and containers (colloquially "cloud" services)
* Networking
* Some security (though this is separate from the cybersecurity unit)

### [Basic cybersecurity skills](topical-units/security)
* Social engineering
* Ethical hacking 
* Best practices

### [WebDev 1: LAMP](topical-units/webdev-lamp)
* HTML
* CSS
* JavaScript
* PHP

### [WebDev 2: NodeJS](topical-units/webdev-nodejs)
* NodeJS
* NPM
* Routing

### [Programming Environments - C](topical-units/env-c)

### [Programming Environments - SQL](topical-units/env-sql)
* Installation
* Working with data (`.csv`, `.sql`)
* Permissions

### [Programming Environments - R](topical-units/env-r)
* RStudio
* R Terminal console
* ggplot

### [Programming Environments - Python](topical-units/env-python)
* Jupyterhub
* Programming basics
* Not applicable if you have taken CS128 at Earlham

### [Programming Environments - Java](topical-units/env-java) 
* Fulfills 3-2 pre-engineering requirement
* Compilers / Interpreters

### [Programming Environments - Android](topical-units/env-android)
* Android Studio (IDE)
* XML
* Java
* MySQL

### [Programming Environments - Arduino](topical-units/env-arduino)
* IDE Installation
* Sensors and values
* LCDs and Display

### [Programming Environments - OpenMP](topical-units/env-openmp) (Under Construction)


