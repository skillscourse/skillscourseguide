# CS Fundamentals Unit

This is the canonical source of information for completing the CS fundamentals unit. This is the first unit you will complete in the course. You should spend approx. 3.5 weeks on this unit so that you have time to complete your topic module. 

The rest of this README will describe the process and expectations for completing the base unit.

## 1. Setting up your workstation

The first task is to setup a dev environment compatible with the workflow we expect for this project. This will be your laptop, desktop, or a Mac laptop you checkout from the CST. 

**Vocabulary note**: In this context, "your workstation" refers to whatever computer you are using to work on your project. In most cases, will be your local laptop, but it may also be a library-loaned laptop or a CS server. You may edit, build, and run your software projects on your choice of workstation initially. Later stages involving batch job submission will will require your code to build and run on a cluster or server in the CS world (one of: Faraday, Hamilton, Whedon). For the purposes of this class you will either use the Terminal application under OS X, or the Windows 10/11 terminal. 

Setting up WSL is not required, but if you are having trouble using the terminal on Windows, instructions and some additional options can be found here: [Instructions for Windows users here](https://wiki.cs.earlham.edu/index.php/Notes_for_Windows_users).

#### Make sure you can open the terminal and connect via `ssh` to your CS account on both `bowie.cs.earlham.edu` and `cluster.earlham.edu`.

## 2. Setting up an SSH Key

In this step you will setup your ssh key so you don't have to keep typing your password over and over. Using ssh keys is both easier and more secure than typing your password. You'll want an ssh key, both for connecting to our servers and for pushing code.

If you are reading this, your CS account is already working. Hold onto those credentials, because you will need them to access all services we run.

* Generate or find your SSH key
  * SSH keys can be found in the `.ssh` folder in your home directory. On your local machine, your "home directory" is probably the one that contains your "Documents", "Pictures", "Downloads", etc. On our servers, your home directory is the one you start out in when you SSH to the server. the `.ssh` folder is hidden by default, but can be `cd`'d into (`cd .ssh`). If you have a key, this folder will contain a file called `id_rsa.pub`, otherwise you will need to follow [this guide](https://wiki.cs.earlham.edu/index.php/How_To_Set_Up_SSH_Keys) to set up your key.
* Add your SSH key to your gitlab account:
  1. On your workstation or laptop, generate an ssh key or copy the `.pub` file for your existing key. SSH stores key files in the directory ~/.ssh (that is a directory named .ssh in your home directory). 
  2. In the upper-right corner, click your avatar (probably a colorful texture of some kind) and then "Preferences".
  3. Left sidebar > "SSH keys"
  4. Paste the text into the big box. Change the title if you want.
  5. Click "Add key".

## 3. Setup your local development tools 

OSX users should install X Code following Apple's instructions. Linux users should install the GNU compiler suite and development tools package. Windows users have a few options. Powershell is likely the easiest way to get started, and [you can learn more about the options here](https://wiki.cs.earlham.edu/index.php/Notes_for_Windows_users). The most important tools on your local machine right now are `git` and `ssh`.

A reference from our wiki [a page about setting up your local computer to interoperate with our services](https://wiki.cs.earlham.edu/index.php/How-To). 

Your first assignment, which will test that you have correctly set up your repository and your local git instance, is to create your `skillscourse` project (and, by extension, its repository).

## 4. GitLab Project
Create your GitLab project. A GitLab project includes the repo as well as issues, members, and other features that are not part of git itself.
* Navigate to the GitLab homepage. You can get here by clicking "GitLab" in the upper left or by going to [https://code.cs.earlham.edu/](https://code.cs.earlham.edu/).
* Create a new project.
* Name your project with your full name plus "266". It might look like "PorterLibby-266".
* Make a description with the text "My Skills Course project".
* Keep the project marked as private.
* Uncheck "Initialize repository with a README" box. You will be making your own README, so you won't need the template.
* Click "Create project".
* Go to your project's main page
* Left sidebar > "Members"
* Under "Invite member" search for the course instructor's name and add them as a "Maintainer" (leave expiration date blank)
* Click "Invite"
* Return to your project's home page.

## 5. Git Repo
This is where you'll set up to begin coding.

* Clone the project to your workstation over HTTPS using the command `git clone [repository url]`.
* In your favorite text editor, create a README as follows (take a look at [this syntax guide](https://www.markdownguide.org/cheat-sheet/)):
    * Your name, formatted as a title
    * In the body, a complete sentence containing your name, year, and major
    * The subheading "My Topics"
    * A single bullet point with the text "CS fundamentals"
* Save and close the file.
* Add the file to git with `git add *` or `git add README.md`.
    * If you're on Windows and your git commands aren't working, you will want to go back [to this page](https://wiki.cs.earlham.edu/index.php/Notes_for_Windows_users) and follow the instructions about installing git.
* Commit the file with a short message (`git commit -m "initial commit, adds readme"`)
* Push the file to the remote with `git push`.
* To make sure you never (or only once) have to enter your password again, run the following:
    * `git config --global credential.helper store`

## 6. Set up remote file editing

Set up remote editing. [Here is our wiki page about this.](https://wiki.cs.earlham.edu/index.php/Remote_file_editing)

VSCode is recommended, and you may already have it set up from a previous CS course. 
Make sure you can access the CS servers and your home directory from your remote editor.

Do not proceed until you have completed local dev environment setup.


# Assignments
The remainder of the base unit consists of these sub-units:

1. [Command line utilities and Bash](command-line.md)
2. [Introduction to regular expressions](regular-expressions.md)
3. [Version control with Git](git.md)
4. [Markdown for composing readable text](markdown.md)
