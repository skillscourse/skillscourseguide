# Sub-Unit: The Linux command line interface

Linux is the family of free-and-open-source (FOSS) operating systems that are run widely on what is today known as "the cloud". You may have heard of Linux distributions ("distros") such as Debian, Ubuntu, RHEL, CentOS, Mint, or Arch. Our servers all run one of these.

Linux distros have graphical user interfaces (GUI's) of the kind you are likely accustomed to on a desktop, laptop, tablet, or phone. However, in this course we will not use these. Instead you will develop a proficiency in using the Linux command line interface. (Using this will make you look like you're in a hacker movie.)

## Background
### The Unix philosophy

The Unix philosophy is to develop simple tools that accomplish one well-defined task very well, and then to build more complex operations by stitching those tools together.

### Unix vs. Linux

The original Unix operating system was a proprietary OS developed at (and owned by) AT&T starting in 1969, known later as Sys5. It quickly forked and an open version became available soon thereafter, Berkeley Systems Design or BSD. While they shared underlying approaches their system call interface was not compatible. In the 1990s the two worlds began to merge with the emergence of Solaris from Sun Microsystems. This was a dual-universe kernel that supported both call interfaces. The ISO eventually standardized the Un\*x call interface and core utilities in the POSIX specification. 

The Linux kernel built on this foundation, and with the help and utilities of the GNU project quickly developed a POSIX compatible operating system. BSD has also continued to evolve, with Darwin forming the core of Apple's OSX operating system. 

Windows is neither a Unix nor a Unix-like OS. However, they now have features (e.g the Windows Subsystem for Linux and the Windows Terminal) that make it possible to use Windows for many of our purposes. In the future Windows will be an interface running on a Linux kernel ala Gnome. 

For practical purposes, we will most commonly refer to the Un\*x *philosophy* but the Linux *kernel* and Linux *operating system* family.

### Bash

Bash, or the Bourne Again SHell, is the default shell on all our servers and therefore for this course. (This is not true for all computers and contexts. On OSX for example zsh is often used, but it's easy to change to Bash). A shell is the command line user interface for the operating system on a computer. The shell may load a number of environment variables, some widely used (e.g. $HOME) but others specific to the user or session.

To discover what shell you are using, run `echo $0` in a terminal.

## Learning Goals
* Solidify what you learned about Bash in CS128
* Understand what stdin, stdout, and stderr are 
* Command line recall - ^a, ^e, ^r, history
* Pipes, redirection - capturing both stdout and stderr to one file
* For loops - counting, over a list of files 
* Variables
* Modules, including the commands avail, list, load, and unload. 
* Scripts
* Commonly used commands: tar, ps, cat, grep (egrep), sed, awk, split
* Job control with nohup, &, ^z, bg, and fg
* Editing variables in-line, e.g. in a for loop to manufacture a new file name with the same base:
	1. ${var#\*SubStr}  # drops substring from start of string up to first occurrence of `SubStr`
	1. ${var##\*SubStr} # drops substring from start of string up to last occurrence of `SubStr`
	1. ${var%SubStr\*}  # drops substring from last occurrence of `SubStr` to end of string
	1. ${var%%SubStr\*} # drops substring from first occurrence of `SubStr` to end of string
* Automated testing, see the files in [command-line-examples](command-line-examples) Note that the easiest way to obtain these examples (and all the others) is to just clone the class to your local machine.

## Resources
* [CS128 Shell Lab](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/blob/master/cs-fundamentals/Lab8-Shell-F21.pdf) Note: You will have to obtain the data files referenced in the lab from https://cs.earlham.edu/~charliep/courses/cs128
* [Software Carpentry Tutorials](https://swcarpentry.github.io/shell-novice/)
* [Basics of Bash](https://towardsdatascience.com/basics-of-bash-for-beginners-92e53a4c117a) (Requires Medium account; you can skip this one.)
* [How to Bash](https://towardsdatascience.com/how-to-bash-for-those-of-us-who-dont-like-the-command-line-8cbf701356dd)
* [For loops](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html#sect_09_01)
* [Variables](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html#sect_03_02)
* [Scripts](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html#sect_02_01)

## Deliverables
* A simple Bash reference that you develop for yourself. It should include all the commands required to support the learning goals listed, a simple example for each (some examples can cover more than one concept, e.g. a for loop over the files in a directory and variables). It should also include commonly used commands such as tar, ps, cat, redirection, pipes, etc. This should be a plain text file written in markdown. An example can be found [here](bash-reference-example.md) 

* To submit your work add your reference sheet to your gitlab project, commit and push. Go to Moodle and in the appropriate assignment submit the gitlab URL of your reference sheet (hint, this is easy to capture by just viewing the file via https://code.cs.earlham.edu). 

## Examples
* `for size in 100 200 300 400 500; do ./my-program $size; done` - run my-program 5 times with 100, 200, etc. as the input values
* `for filename in *.dat; do echo $filename; echo done` - iterate over all the files with the .dat extension
* `for i in {1..100}; do echo $RANDOM; done | cat -n > file` - make a file with 100 random numbers
* `for x in {1..8}; do echo $x; time ./openmp-demo -t $x; done` - run openmp-demo using 1 through 8 threads with time data collection
