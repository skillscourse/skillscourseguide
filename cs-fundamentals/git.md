# Sub-Unit: Git

Git is a [distributed version control system](https://en.wikipedia.org/wiki/Distributed_version_control). It allows users to manage and track versions of files as they are changed and updated, an important tool for programming projects, particularly where multiple people are involved in creating code. There are many different kinds of version control system, but most now use git to manage source code. 

## Learning Goals
* Learn to use the git version control system's most common features by using git to manage the development of a program.
* Familiarize yourself with the Gitlab web interface and navigating projects, groups, and repositories.
* Understand the purpose and benefits of using a version control system.

## Resources
- [Github Flow](https://docs.github.com/en/get-started/using-github/github-flow)
- [GitKraken Tutorials](https://www.gitkraken.com/learn/git/tutorials)
- [GitSCM Book](https://git-scm.com/book/en/v2/)

### How it works

Git is a distributed version control system (VCS). Every copy of the code repository contains all the code, including all branches. You and I might both have a copy of the code that we work on independently and then share in GitLab so others can have the features we've added. While we treat GitLab as the canonical host of our files, this is by our choice and design (i.e. it is not imposed by git itself). There is no dependency on e.g. your Internet connection if you have cloned a git repository to your local machine.

You're in a GitLab instance now, and you've likely heard of GitHub or BitBucket. These are all graphical user interfaces and extensions for git. Issues and users, for example, are not part of the repository, but instead extend git to make it more user-friendly.

### How you will use git

All source code for this course will be managed in git. Other courses in computer science at Earlham may also require working knowledge of git. It's almost certain that future employers, research opportunities, and personal projects will make use of git.

More generally, understanding git at a deep level lets you understand the way your code is changing. You should not be in the position of carrying dozens of "archive-YYYY-MM-DD" directories or depending on your own memory to keep track of changes.

### Vocabulary

Some key terms you will hear and read many times in git:
* **add**: stage the changes to a particular file for the next commit
* **rm**: remove a file from code in the next commit
* **commit**: a collection of changes to source code with an ID and (usually) a message; think of this like "saving" your code
* **push**: update the remote version of the repository to include all your local commits; in our case this means "put all my new commits in GitLab"
* **branch**: a named set of code changes; branches are separate from each other, and a common use for branches is to work on something in a space that does not affect the main/master/production branch

As a rule:
* Commit often (small diffs are easier to understand and debug)
* Push occasionally (network is expensive compared to compute)

### WARNINGS

Here are some things you are not to put in git:
* **passwords**: these should be kept in a separate file, not tracked by git, that your program reads
* **binaries**: a git commit should never include the actual program that runs; rather, include the source code and a set of instructions on how to [build and install it](build-and-install.md)
* **big files**: most images, videos, audio, etc. should not be committed to git

## Deliverables

Before starting, make sure you have completed the instructions in the main CS-fun [README](README.md).
You do NOT need to create a new repository, you can use the one you have already created for this class.
You should already have completed these steps, so ONLY DO THESE if you missed that part:
* Create a project in GitLab > Skills Course.
* Add instructors to your project.
* Clone it to your workstation.
* Add your initial README.

After setup, complete the following (This is most likely where you should start):
1. Add a `.gitignore` file that excludes image and video files as per the **WARNINGS** section. Add, commit, and push.
2. Create a new branch called "hello-branch" with the `branch` command and switch to it with the `checkout` command. 
3. Make an edit to a file while in this new branch.
4. Add, commit, push.
5. Check out the page for your repo on Gitlab. You should see a drop-down menu under the repository title with the "main" and "hello-branch" branches.
6. Back on your local copy, you should now `merge` "hello-branch" back into main.
7. Add, commit, push.
8. As your deliverable, submit the URL to your commit history showing the new branch, and it's subsequent merge back into the main branch.

This concludes the git-centric subunit, but your work with git will continue throughout this course/your career. Your evaluation for the git unit will also include your git commit history for your project. Using git, writing good commit messages, frequency of adds versus commits versus pushes, etc. will all be considered.
