# Sub-Unit: Learning Goals

* Learn the basics of Markdown.
* Understand its value.
* Learn to use it to write developer documents in future courses, jobs, etc.

## Resources

Markdown is a markup language widely used in developer documentation.

Markdown has several advantages over plain text, rich text, or HTML:

* a well-defined syntax
* requires only a text editor to produce
* widely-used and nicely-rendered across a variety of platforms (GitHub, GitLab, some WordPress environments)
* also readable with no formatting or coloring at all

To see what the last bullet point means, open this page in a text editor or "View raw" in GitLab. You will see that the headings, bullet points, and other syntax structure the document in a legible way with the text alone.

This sort of simplicity and portability has made Markdown THE go-to markup language for developers.

GitHub, which has developed its own "flavor" of Markdown to add a few features for use on its platform. GitLab supports this flavor. [Read here.](https://guides.github.com/features/mastering-markdown/)

## Deliverables

Write out the README in your repository for this course. Include:

* A description of the course
* A section on the README for this unit (fundamental computing skills)
* A list of the skills in this unit
* A working link to the Moodle page
* A section displaying the code for "Hello World" in C

Hint: In doing so, you should find yourself using all of the following:

* Headings
* Bullet points
* [Links](https://https://guides.github.com/features/mastering-markdown/)
* *italic* and/or **bold** text
* A properly-formatted code snippet

Hint: on GitLab, there is a blue "Edit" button on the page for every file, and that button has a "Preview" feature. Use this if you wish to compose Markdown with easy previewing and updating.
