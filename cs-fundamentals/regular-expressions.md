# Sub-Unit: Regular expressions

Regular expressions are an extremely powerful method for finding and manipulating strings. They act as wildcards to match more than one string, filename, etc. Patterns, sometimes called globs, are a subset of REs that are used by many command line programs.

Most developer text editors have support for regular expression searches, for example, and programs like `sed` and `awk` rely on them heavily. Any program that accepts one or more files on the command line also supports patterns, e.g. `ls *.py` displays all of the files whose extension is .py.

Note: This topic unit is about the practical application of a particular kind of regular expression, aka patterns or globs, as distinct from regular expressions in theoretical computer science.

## Learning Goals
* Learn the basics of patterns and regular expressions as they are used by grep,egrep, ls and other command line tools.
* Prepare to learn how patterns and regular expressions are applied within languages such as Python and databases such as Postgres.

## Resources
* [FLOSS manual for Regular Expressions](https://archive.flossmanuals.net/command-line/regular-expressions.html)
* [Guru99's RE Tutorial](https://www.guru99.com/linux-regular-expressions.html)

## Deliverables
* Add a number of examples of common patterns and commands with patterns that you might use to your Bash reference sheet. 
* Practice using egrep with the data files in the [command-line-examples](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/tree/master/cs-fundamentals/command-line-examples) directory to search for numbers with specific patterns (start with 5, end with 12, has a 66 with at least one character on either side.) Document these expressions in your reference sheet.

* To submit your work you can resubmit the same URL as you did for the command line sub-unit, only now with updated content. 

## Examples
* `ls 5*.py # list all the files that start with '5'`
* `cat homework?.py # display all the files that start with homework followed by 1 character`
* egrep, or grep -E:
	1. `egrep -r search_string *` - search all the files in the current directory and subdirectories for search_string 
	1. `egrep -i search_string *.dat` - case insensitive search
	1. `egrep -w search_string file_to_search` - search for full word only and not as sub-string
	1. `egrep -l search_string *.dat` - list only the file names that contain a match, not all the matches themselves
	1. `egrep -o search_string *.dat` - list only the matched string, not the full line it appears on (super handy for counting occurences when piped into wc -l)
