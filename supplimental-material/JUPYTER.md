# Using Jupyter / Jupyterhub (Suppliment)
This page will guide you through the process of getting started with Jupyterhub, which we host here on campus for students to use for programming in Python and other languages.

## Access
Navigate to [juypter.cs.earlham.edu](https://jupyter.cs.earlham.edu) in a browser, and log in with your CS credentials (these are separate from your Earlham credentials that you use to log into Zimbra or Moodle). If you don't have a username/password for CS, get in touch with your instructor, or email admin@cs.earlham.edu so that we can set one up for you.

Once you log in, you should see a drop-down menu with options. You can use the default profile: `2GB RAM, 0.5 Core (CS128)`. Click `Start`. After it loads for a bit, you should be able to start programming.

## Using the Jupyterhub interface
Jupyter has a very nice page with a lot of details about the interface [HERE](https://jupyterlab.readthedocs.io/en/4.1.x/user/interface.html), if you'd like more information.

A quick synopsis of the interface:
- `File Browser` (Left edge of the screen): This works a lot like the filebrowser on your computer. You can use it to create new files and folders, organize, delete, and rename. This filebrowser includes only remote files that you are storing on the CS server, and does not access or use the local files on your laptop.
- `Main Work Area` (Right side of the screen): The majority of the screen is taken up by the work area, which is where you'll be editing code and files. It should start out by displaying a "Launcher", which gives you options for several different versions of Python, as well as some other languages.


## Running Python Code
Start by selecting "Python 3.12" from the launcher. This will start a new, blank Notebook in the work area. Try typing `print("Hello World!")` in the notebook, and clicking the little "play" button at the top of the Notebook (you can also use `shift`+`enter` to run your current cell)

When you run the "Cell", it should look something like this:
```
[1]: print("Hello World!")
```
`Hello World!`

Each "Cell" can contain python code, and when it is run, the entire cell will be run all at once. Any output from the code in that cell will be put below it (in between the current cell and the next). Mutiple cells can be used to space out your code, and add space for output from your code. Looking at a large amount of code can be much easier when it is divided up into chunks. The number to the left of each cell represents the order that they have been run in. Notice that re-running your cell will increment the number each time.

You can also use Jupyter to write and edit a normal `.py` file, which python code outside of a notebook. All the same code will work in a `.py` file, just without the added interactive elements of the notebook environment.