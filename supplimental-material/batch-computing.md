# Batch Computing (Suppliment)

Many programs will run just fine on your laptop or through some web interface. However, for large, complex programs that require large amounts of data and take on the order of hours or days to complete, it is better to run the job as a batch through a scheduler. The scheduler can take your program/script and run it over a long period of time without you having to supervise it. It can also run a script many times in parallel.

## Learning Goals

- Learn the basics of batch scheduling.
- Learn the commonly used commands for Slurm.

## Resources

- [Using Slurm (CS Wiki)](https://wiki.cs.earlham.edu/index.php/Getting_started_on_clusters#Using_Slurm)

Earlham CS hosts instances of the Slurm scheduler on each of its `cluster.earlham.edu` systems except for Hopper (e.g. Hamilton, Whedon, Faraday). If you see documents mentioning `qsub` or Torque, they are likely out of date, as we no longer host any Torque instances. Slurm can run some Torque scripts, but it is better to prepare your job to run specifically in Slurm.

How to Connect to Whedon/Hamilton/Faraday:
You can SSH to these machines by first connecting to Hopper, and then to either Hamilton (`hamilton.cluster.earlham.edu`), Wheden (`whedon.cluster.earlham.edu`), or Faraday (`faraday.cluster.earlham.edu`).

## Common Slurm Commands
- `sinfo` - Gives information about the different available queues (called partitions), what state they are in, and which machines are running jobs for that queue. 
- `squeue` - Shows all jobs currently running in any partition/machine in the cluster. 
- `srun` - Run a single command through the Slurm scheduler.
  - `srun --pty bash` - create an interactive bash session as a Slurm job on one of the available compute nodes.
  - `srun python helloworld.py` - Run a script called `helloworld.py` on an available compute node.
- `sbatch` - run an sbatch file, which creates a job similar to `srun` using the parameters specified in the sbatch file.

## Example file: `my-slurm-script.sbatch`
```
#!/bin/sh
#SBATCH --time=20
#SBATCH --job-name hello-world
#SBATCH --nodes=1 
#SBATCH -c 1 # ask for one core
#SBATCH --mail-type=BEGIN,END,FAIL 
#SBATCH --mail-user=excellent_email_user@earlham.edu

echo "queue/partition is `echo $SLURM_JOB_PARTITION`"
echo "running on `echo $SLURM_JOB_NODELIST`"
echo "work directory is `echo $SLURM_SUBMIT_DIR`"

sleep 10           # Replace this sleep command 
```
## Practice Exercise 

Use one of the following srcipts as the basis for your own program. Compile and run it interactively varying the input until you understand how it scales based on the different parameters. 

- [auc-trapazoidal-serial.c](../topical-units/env-c/c-examples/auc-trapazoidal-serial.c)
- [primes.c](../topical-units/env-c/c-examples/primes.c)

Experiment with differnet choices for a, b and n until the job takes about an hour to run on a machine like Hamilton or Whedon. You can time the job by putting `time` in front of your other commands on the command line.

Once you know values to use so that it will take about an hour to run normally, you are ready to move onto the following step.

When you are confident you have proven your program is correct, prepend it with the `time` command and submit it to Slurm using `sbatch`. **Run it on Hamilton, Whedon, or Faraday**. You can connect to lovelace (and all the other cluster nodes) by first ssh'ng to cluster.earlham.edu and from there to any of the other cluster nodes. 
