# Building and Installing Software (Suppliment)
## Learning Goals

In this unit you will learn common methods of building and installing software.

## Resources
- https://makefiletutorial.com/

### The GNU Build System
The GNU Autotools/GNU build system is a widely-used set of tools that makes it possible to build and install binaries from source code on a wide set of systems.

A common example might be (`$` represents a new line in the terminal):
```
$ ./configure
$ make
$ make install
```

This course will focus only one of those steps, `make`.

Note: This is not the only automated build system but it is widely used and it is one we will use.

### GNU Make and Makefiles

You might build a C program by running `gcc hello.c -o hello`, which isn't so tricky. Suppose, though, that you want to instead use the `mpicc` compiler and link some math and parallelization libraries. That command might look like this: `mpicc -o hello hello.c -lm -fopenmp`.

Further, suppose you have a collection of binaries to build from C code, such that you must run many of those. Finally, consider there may be a bug in your program and you may need to rebuild all or some of the binaries after you fix it.

For these not-uncommon cases, where complexity quickly builds, memorizing or relying on your shell history for the commands doesn't scale well. Enter GNU Make, and in particular the Makefile. A good introduction to Make and Makefiles can be found in the [GNU documentation](https://www.gnu.org/software/make/manual/html_node/Introduction.html) and an example Makefile can be found in the [build and install examples](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/tree/master/cs-fundamentals/build-and-install-examples)

In a Makefile, you can specify the formulas for building your programs, including choice of compiler and libraries to link. You may also specify the recipes for related operations like `make all` (usually meaning "build all the objects possible") and `make clean` (usually "erase all the built objects so I can restart").

### Package Managers

Most operating systems have a widely-used package manager associated with them. For example:
* Debian: `apt`
* CentOS: `yum`
* macOS: `brew` (separately installed but widely used by convention)

If you have your own Linux system you should read the man page the appropriate package manager to install a new package or list the installed packages. If you do not, read the Wikipedia page on [package managers](https://en.wikipedia.org/wiki/Package_manager) through Repositories.

## Practice Exercise 

Create the following:
* a Makefile with options for "all", "clean", and your executables
* demonstrated ability to build and run on at least two machines (e.g. a Mac and a CentOS server), you are on the honor system for testing your Makefile on at least two machines although we may ask for a demonstration.
  * Hamilton and Whedon would be good choices, Hamilton uses Debian, Whedon uses AlmaLinux.
* there is no specific deliverable for the package managers portion

Hint: "adds Makefile" or "add Makefile" might be a good commit message...
