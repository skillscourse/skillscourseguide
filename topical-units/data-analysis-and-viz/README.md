# Data analysis and visualization Topical Unit
This unit introduces widely-used skills for gathering, analyzing, and visualizing datasets. These will include Python (including the pandas, numpy, and MatPlotLib libraries) and R (including the ggplot library). The project will consist of statistical analyses, visualizations, and written descriptions of data. You have the option of choosing (finding, cleaning, storing, loading) a dataset in a domain of your interest, or of using data already available through Earlham.


Skills will include...
* Python
* Python libraries: matplotlib, seaborn
* R
* Visualizations and text descriptions

# Resources

We host multiple data analysis and visualization tools.

* If you want to iterate quickly and learn with relatively small (megabytes to single-digit gigabytes) datasets, the [CS Jupyter environment](jupyter.cs.earlham.edu) has what you need. Both Python and R are supported there.
* If you are working with large datasets or want to begin specializing in something like bioinformatics, you will probably want to go to [Bronte](bronte.cluster.earlham.edu), a cluster server. Its notebook environment also supports Python and R.

If you're unsure, default to the CS Jupyter environment.

# Learning Goals

By the end of this unit, you will know how to get data and produce basic data visualizations. You will also learn some of the statistics that underlie data science. This is not a substitute for a course in data science that goes in-depth on these topics, but it will significantly reduce the amount of friction between the theoretical material and the generation of useful artifacts.

# Deliverables

You will build a series of data visualizations (almost) from scratch.

You may bring your own dataset, or you may start by looking at [some datasets](http://cluster.earlham.edu/datasets) we have curated. We also have a CSV file of [local climate data](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/blob/master/topical-units/data-analysis-and-viz/richmond-indiana-water-works.csv). (If you bring your own and prove it's substantive, we will add it to that webpage.) If you need to store a big dataset (>10s of GBs), check with an instructor first.

Here are the data analyses and visualizations we need to see by end of term:
* Load the data as a Pandas dataframe and get some stats from it (e.g. mean, median, mode, range over some index, and more).
* Create a Matplotlib visualization comparing a few categories within a dataset (could be a line graph, bar graph, box-and-whisker, or another visualization that fits the data).
* Create an equivalent or comparable visualization to your Matplotlib visualization using R.
* Using either Matplotlib or R, create another visualization showing different data or the same data in a different way.
* For each visualization, write a paragraph (3-5 sentences) explaining it.
* Make your visualizations look good! (Really, this will be part of your evaluation.)
* Turn-in the URL of your gitlab directory for this project to Moodle.

Two servers, [bowie](https://jupyter.cs.earlham.edu) and [bronte](https://bronte.cluster.earlham.edu), have notebook environments for Python and R. They should have all relevant libraries, but if we've missed one we can install it quickly.

## An important note about git
Manage your code through git. Do not put your *dataset* in git, as it is likely too large to be reasonably moved around the Internet. **Be careful** when using a notebook that you only commit files to git after running "Restart and clear output" and then saving the file. Otherwise, you may accidentally commit a much larger amount of data to git than you should.
