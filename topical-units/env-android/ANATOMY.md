# Android Anatomy
Android can be a daunting IDE to work with. Use the example below as a reference for what different files do, and how you can use them. 

Keep in mind that for the deliverables specified on the main README.md of this module, the most important parts are the `MainActivity` Java file, and the `res/layout` folder, containing your XML layouts for the GUI of the app. Even if you are hoping to accomplish something more complex, the best place to start is with these couple of files, and work your way outward from there.

**Note:** for the sake of this example, the project is called "CS266" and was created using the "Empty Views Activity" template, using Java as a language (Instead of Kotlin). A Kotlin project will look a little different, but include many of the same components.

If you are still feeling lost, please ask quetsions!

## Breakdown of a Sample Project
### **manifests/**
  - **AndroidManifest.xml** - Essential information about your app and how it is built. More info [Here](https://developer.android.com/guide/topics/manifest/manifest-intro).
### **java/**
  - **com.example.cs266/**
    - `MainActivity` - This Java file contains the code being run when the app is launched from the homescreen. It should have an "onCreate" method that will set the view to an XML layout from the **layout/** directory.
  - **com.example.cs266/ (androidTest)**
    - `ExampleInstrumentedTest` - This Java file contains unittest code that will be executed on an Android device.
  - **com.example.cs266/ (test)**
    - `ExampleUnitTest` - This Java file contains unittest code that will be executed on the host machine running Android Studio.
### **res/**
  - **drawable/**
    - `ic_launcher_background.xml`
    - `ic_launcher_foreground.xml`
  - **layout/**
    - **activity_main.xml** - This XML file contains the layout for the default app. You can have multiple different XML layouts in the **layout/** directory. More info about layouts [Here](https://developer.android.com/develop/ui/views/layout/declaring-layout).
  - **mipmap/** - Contains all the XML information for app icons/launchers. There are multiple versions for different devices, resolutions and shapes.
    - **ic_launcher/**
    - **ic_launcher_round/**
  - **values/** - This directory contains XML files that are used to configure project-side variables. It is commonly used for naming things, and for storing color information. It also contains the subfolder for themes, which can make use of those colors to create different color scheme combinations.
    - `colors.xml`
    - `strings.xml`
    - **themes/**
      - `themes.xml` - Note that this is referenced in the AndroidManifest.xml
      - `themes.xml (night)`
  - **xml/** - These files contain rules about specific aspects of the app. They are also referenced in AndroidManifest.xml. For most basic apps, there is no need to modify these rules.
    - `backup_rules.xml`
    - `data_extraction_rules.xml`

## Gradle
In addition to the file structure above, you will see a list of "Gradle Scripts". Gradle is the system by which Android Studio compiles and packages your code into a usable application that can be installed on a phone. The files in this section control settings about how your app is built. 

For a simple project, you are unlikely to need to change anything in these files, but it is still good to have a basic understanding. It is highly recommended to explore them a bit, see what settings are available, and ask questions if you have them. 

The most commonly used of these files is `build.gradle (Module :app)`, which contains a lot of settings about the minimum and maximum versions of Android your app can be used on, and which version of Java it uses.