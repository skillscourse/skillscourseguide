# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://seeklogo.com/images/A/android-logo-9E4539A7DE-seeklogo.com.png"> Android Topical Unit

Android development consists of either Java or Kotlin, the MySQL database, and XML styling, as managed through the Android Studio application (available for all supported platforms). For this unit, you will work on creating a simple Android application.

Kotlin is a newer language that is fully compatible with Java. Any library or code written in Java can be used in Kotlin programs. It is becoming increasingly popular in Android development because of its improvements over the Java language.

You may use either language, though you might prefer Java if you have already taken the Java module offered in the course.

**Note 1:** If you have any trouble running Android Studio, or the Android Emulator (both of which can be resource intensive), a lab machine in Lovelace might be a good option for you. 

**Note 2:** If you do not have an Android device, and would like to test your app on real hardware, you can borrow one to use for the duration of the module.

# Learning Goals
- Understand the basic structure of an Android Studio project, including the different files and folders.
- Understand the basic components of an Android application, such as activities, layouts, and resources.
- Use the Android emulator to test the application on different devices.
- Explore the Android Studio documentation to learn more about advanced topics such as user interface design, data storage, and networking.

# Resources
- [Android Studio IDE](https://developer.android.com/studio)
- [W3Schools - Android](https://www.w3schools.blog/android-tutorial) - Lots of information about specific components of Android development
- [W3Schools - Kotlin](https://www.w3schools.com/kotlin/index.php) - Lots of good information about how to use Kotlin.
- [Android Emulator Developer Guide](https://developer.android.com/studio/run/emulator)
- [Android Anatomy README](ANATOMY.md) - Breakdown of the different files in a sample Android project.

# Deliverables
You will write a "hello world" Android app and install it on your own device, or on the Android Emulator included with Android Studio.

Your code should have three components:
* a main screen that reads "Hello world"
* an "About" screen (accessible through a button) with your name and graduation year
* an "Exit" button that closes the app

A few specifications:
* Your IDE will be Android Studio (available for all OS's).
* Choose a custom background color or image.
* As always, manage this through git.

