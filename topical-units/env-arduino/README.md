# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://seeklogo.com/images/A/arduino-logo-BC7CBC1DAA-seeklogo.com.png"> Arduino Topical Unit

Arduino hardware and the associated principles are the basis of much of what is called the Internet of Things. The Arduino/C integrated development environment supports construction of sensors and actuators that let you interface software/hardware systems to the physical world. 

# Setup

1. Check-out an Arduino development kit from the CS Department. If you are using your own kit make sure you have the correct USB cable for your Arduino board/computer combination.
1. Download and install the Arduino integrated development environment (IDE): https://www.arduino.cc/en/Main/Software 
1. Plug-in your board with the serial cable.
1. Launch the Arduino IDE.
1. Check/set Tools-Board and Tools-Port as appropriate for your board/machine.
1. Check that Tools->Get Board Info returns what you think it should.
1. Open File->Examples->Basics->01.Blink. Verify and upload the script to your board. Do you see the appropriate LED blinking?
1. Unplug your board, close the IDE, do something else for a bit. Plug it all back in and make sure you can run the basic blink again. Order often matters, e.g. having the board plugged-in before launching the IDE makes a difference on some platforms. 
1. Change the ratio of on/off time of the LED. Verify, upload, and test. 
1. Change the pattern of blinking to be SOS in Morse code, with a two second pause in-between transmissions. 
1. Check-out a USB battery and cable from the CS department. The Arduino saves the last sketch uploaded to it in non-volitaile memory and will run it when powered-up. Repeat steps 8 through 10 only now after uploading a new sketch disconnect the board, power it up on the battery, and verify that the correct script is working the correct way. 
1. Serial console and graphing, start with internal temperature. 
1. Find some hardware (photoresistor and resistor), plug it in, write software to work with it.

# Resources

1. Documentation in the examples library.
1. https://www.youtube.com/watch?v=z9PPfE8dvGs Using a photoresistor (hardware available from Craig/Charlie)
1. URL Using an LCD display (hardware available from Craig/Charlie)

# Learning Goals

After this unit you will be able to work on the Arduino platform to create basic hardware-software projects.

1. Connect Arduino boards and configure the IDE.
1. Display values from internal sensors on the serial console and plotter vs time
1. Configure a photoresistor and plot the value over time.
1. Configure an LCD, display the most recent value averaged over 3 second intervals.

# Deliverables

1. Final sketch for the SOS project uploaded to your gitlab project.
1. Sketch for the internal sensors project uploaded to your gitlab project.
1. Sketch for the photoresistor project uploaded to your gitlab project.
1. Sketch for the photoresistor and LCD project and a picture of the hardware uploaded to your gitlab project.