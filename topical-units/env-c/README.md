# <img width=28px height=30px style='margin-right: 10px; margin-bottom: -5px;' src="https://upload.wikimedia.org/wikipedia/commons/1/19/C_Logo.png"> C Topical Unit

C is a procedural programming language that gives developers a high degree of control over resource consumption. It is widely used in high-performance computing, scientific computing, embedded systems programming, operating systems, and other contexts where performance is critical. Numerous other languages are derived from C (e.g. C++), written in C (e.g. Python), or in important respects comparable to C (e.g. Go). Earlham CS in particular uses C programming often and has resources that can take advantage of its approach. As such, it is a valuable language to learn.

This course emphasizes learn-by-doing, so to learn C you will write programs in C. The way to learn a programming language is to read and write code in that language. 

You will write in C, manage versioning through git, navigate files in a Linux OS using Bash, and learn to build software using the GNU auto-tools. We'll explain all of these and provide further resources as you go.

## Resources

This section contains resources for learning C language programming under Linux, OS X, and possibily the Windows+Linux subsystem. Some people prefer reading, some watching, some listening, some a mix.

* The C Programming Language 2e, Kernighan and Ritchie. - The canonical reference, contains many exercises ranging from simple to complex. (todo - tag a reasonable set)
* Example code found [here](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/tree/master/cs-fundamentals/c-examples)
* [TutorialsPoint](https://www.tutorialspoint.com/cprogramming/index.htm) - Both video and text.
* Programiz 

## Learning Plan 

The plan for learning a language follows from the common elements that programming languages share. Find the section in the resource(s) that you are using that covers each of these topics.

* Types, operators, and expressions
* Control flow 
* Functions and program structure
* Pointers and arrays
* Structures
* Input and output

## Getting Started 

You can either install a C compiler on your own machine or use one of the CS servers. These instructions are for gcc, the GNU C compiler, which is at the core of XCode under OSX, WSL under Windows, and is available on every Linux system. 

1. ssh to bowie.cs.earlham.edu 
1. Open a new file named hello-world.c using either a local or remote editor.
1. Follow the instructions here: [Hello world](hello-world.md). You will likely need to do some research about how to write Hello World in C if you haven't used C before.
1. Compile the program: `gcc hello-world.c -o hello-world` 
1. Run the program: `./hello-world`
1. When you are ready to use command line parameters there is example code [here](c-examples)

## Deliverables

Write code, here is a provisional list of programs to develop:

1. [Hello world](hello-world.md)
1. Write a slightly more complex C program. 
* Read a file containing an arbitrary number of positive integers, one per line. Display the count, total, and average on stderr at the end of the run.  
* Use a function as part of your program, e.g. to write the aggregated values to stdout. 
* Use command line parameters available via argv and argc to support -i input-file-name to pass the input file to your program. 
* Super optional extra credit for people that have had POCO: Make the program capable of displaying the maximum values of each quantity without resorting to arbitrary precision arithmetic (hint, unsigned long int).


