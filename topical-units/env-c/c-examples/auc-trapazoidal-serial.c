/* 
  auc-trapazoidal-serial.c (based on Pacheco's version) - calculate the area under 
  a curve using a completely serial algorithm. 

  $ auc-trapazoidal-serial -a range-start -b range-end -n number-of-slices
  defaults: a = 2, b = 20, n = 100
  function: f(x) = x^2 + 3.5x - 5
  b must be > a
  n must be > 0

  future enhancement - function vector to support multiple functions to evaluate 
*/ 

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <getopt.h> 

double theFunction(double x); 

int main(int argc, char *argv[]) { 
  unsigned long long n = 100; 
  int i, debug = 0, opt = 0;
  double a = 2, b = 20, h, x_i, area; 

  while ((opt = getopt(argc, argv, "da:b:n:")) != -1) { 
      switch (opt) {
          case 'd':
              debug = 1;
              break;

          case 'a':
              a = strtoul(optarg, (char**) NULL, 10);
              break;

          case 'b':
              b = strtoul(optarg, (char**) NULL, 10);
              break;

          case 'n':
              n = strtoul(optarg, (char**) NULL, 10);
              break;

          case '?':
          case 'h':
          case 'H':
              fprintf(stderr, "usage: -a <range-start> -b <range-end> -n <number-slices>\n"); 
              fprintf(stderr, "without any arguments runs with a=2, b=20, n=100\n"); 
              exit(1); 

          default:
              break; 
      } 
  }

  // Actually do some work.
  h = (b - a) / (double)n;
  area = (theFunction(a) + theFunction(b)) / (double)2.0; 

  for (i = 1; i <= (n - 1); i++) {
    x_i = a + (i * h);
    area += theFunction(x_i); }
      
  area = h * area; 
  
  fprintf(stdout, "a = %lf, b = %lf, n = %llu, area = %lf\n", a, b, n, area); 
  
return(0); }

double theFunction(double x) {
  return((x * x) + x * (double)3.5 - (double)5);
}
