#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  FILE *inputFileHandle = NULL; 
  char inputFileName[32] = "test-int.dat"; 
  int value = 0; 
  int sum = 0;
  int count = 0;
  float average = 0.0; 

	if ((inputFileHandle = fopen(inputFileName, "r")) == NULL) {
		fprintf(stderr, "error opening file: %s\n", inputFileName); 
		exit(1);
	}
  
	while (fscanf(inputFileHandle, "%d", &value) != EOF) {
		count++;
		sum = sum + value; 
	}

	fclose(inputFileHandle); 	
	
	average = (float)sum / (float)count; 
	fprintf(stdout, "file = %s, count = %d, sum = %d, average =%8.2f\n", 
	  inputFileName, count, sum, average); 
	
	return(0); 
}
