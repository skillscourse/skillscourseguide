/* Simple command line argument processing example. */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG

int main(int argc, char *argv[]) {
        FILE *infile;
        char infilename[128];

        #ifdef DEBUG
        printf("argument count: %d\n", argc);
        printf("command: %s\n", argv[0]);
        #endif 

        if (argc < 2) {
                printf("usage: fopen <infile>\n");
                exit(1);
        }

        strcpy(infilename, argv[1]);

        if ((infile = fopen(infilename, "r")) == NULL) {
                printf("Error opening file %s\n", infilename);
                exit(1);
        }

        fclose(infile);
        exit(0);
}

