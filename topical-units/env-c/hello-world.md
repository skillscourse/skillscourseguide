# Hello World in C

You are likely familiar with "Hello World" as the traditional first program to write in any language. Your first assignment is to write "Hello World" in C.

In doing so, do the following:
* In your personal project, open a GitLab issue named "Write "Hello World" in C". Add this bulleted list to the description field.
* Go to your skills course git repository on your workstation.
* Write the program.
* Build it using gcc on the command line.
* Add and commit *the code but not the binary* to your repository.
* Push to GitLab.
* Comment on the issue to ask faculty to look at your commit for reference. Make sure to tag them using `@`.

Use these tools continually as you build. Use issues to track what your code should do, especially challenging bugs you encounter, etc., and tag faculty to ask for help.