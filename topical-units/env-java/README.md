# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://seeklogo.com/images/J/java-logo-7F8B35BAB3-seeklogo.com.png"> Java Topical Unit
## (fulfills 3-2 pre-engineering requirement)
Java is a widely-used object-oriented programming language. It is a prerequisite for some engineering programs that may be available through the 3-2 path at Earlham, and is (currently) the only language you can use to build Android apps. You will build a simple application in Java that will introduce you to the features of the language (types, control flow, etc.), much as you did when learning C in the base unit. One of Java's language design criteria was that it should be easy for people who know C to learn, which among other things means that operators, assignment, etc. should all look familiar. 

This course emphasizes learn-by-doing, so to learn Java you will write programs in Java. The way to learn a programming language is to read and write code in that language. 

You will write in Java, manage versioning through git, navigate files in a Linux OS using Bash, and optionally build a simole Android app.

# Resources

This section contains resources for learning Java language programming under Linux, OS X, and possibily the Windows+Linux subsystem. Some people prefer reading, some watching, some listening, some a mix.

* [Think Java 2e](https://greenteapress.com/wp/think-java-2e/) - an excellent open content book from O'Reilly, PDF download 
* [Wikiversity Java](https://en.wikiversity.org/wiki/Java)
* [TutorialsPoint](https://www.tutorialspoint.com/java/index.htm) - read the following sections:
1. Overview
1. Environment Setup
1. Basic Syntax
1. Object & Classes
1. Constructors
1. Basic Datatypes
1. Basic Operators
1. Loop Control
1. Decision Making
1. Numbers
1. Characters
1. Strings
1. Arrays
1. Files and I/O
1. Exceptions
* todo - identify more resources

# Learning Plan 

The plan for learning a language follows from the common elements that programming languages share. Find the section in the resource(s) that you are using that covers each of these topics.

* Types, operators, and expressions
* Control flow 
* Functions and program structure
* Pointers and arrays
* Structures
* Input and output

# Getting Started 

You can either install a Java compiler/interpreter on your own machine or use one of the CS servers. These instructions are for the default Java environment on bowie.cs.earlham.edu. They will also work for Java under OSX. 

1. ssh to bowie.cs.earlham.edu 
1. Open hello-world.java using either a local or remote editor, populate it with the appropriate statements (see below).
1. Compile the program: `$ javac helloWorld.java` 
1. Run the program: `$ java helloWorld`
1. When you are ready to use command line parameters there is example code in the TutorialsPoint section of the same name.

# Deliverables

Write code, here is a provisional list of programs to develop:

* Hello World 
* Read a file containing an arbitrary number of integers, one per line. Display the count, total, and average on stderr at the end of the run. Make the program capable of displaying the maximum values of each quantity without resorting to arbitrary precision arithmetic (hint, unsigned long int). 
* Use a function/method as part of your program, e.g. to write the aggregated values to stdout. 
* Use command line parameters available via argv and argc to support -i input-file-name to pass the input file to your program. 