# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://avatars.githubusercontent.com/u/8496952?s=280&v=4"> OpenMP Topical Unit (Under Construction)

OpenMP is a standards-based mechanism for building C/C++ software that uses shared-memory parallelism to speed-up the work by harnessing additional cores. It is built-in to the compiler and is accessed through pragmas and directives. OpenMP is often used to improve the performance of existing serial software. 
