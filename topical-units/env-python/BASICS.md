# Basic Python Examples

## Comments
```py
print("test one")
# This is a comment that will not affect the code around it
print("test two")
print("test three")     # Comments can also be on the end of a line
#print("test four")
```
Comments are used to insert non-code text into a script. This can be to describe parts of your code and what they do, or quick notes to yourself or others to help organize and annotate. 

In Python, comments are represented with a `#` at the beginning. Any line starting with a `#` will be ignored as the code is run. In this example, the first three print statements will run normally, and "test four" will not be printed, since that line is commented out. 

## Math
```py
print(5 + 7)        # prints 12 (5 plus 7)
print(7 - 5)        # prints 2 (7 minus 5)
print(1 / 4)        # prints 0.25 (1 divided by 4)
print(3 * 5)        # prints 15 (3 times 5)
print(3 ** 2)       # prints 9 (3 squared)
print(10 // 3)      # prints 3 (10 divided by 3, rounded down)
print(10 % 3)       # prints 1 (remainder of 10 divided by 3)
```
Python uses a lot of typical mathematical symbols, and some that are less common. A lot more mathematical operations, such as cosine and log, can be used as well, but are included as functions instead of just symbols.


## Functions
```py
def myFunction():   # Defines a function called "myFunction"
    print("The function has been run")
    print("Anytime myFunction() is run, this text will print")

myFunction()        # Run the function
```
A function is a way to package and reuse a piece of code. In the above example, `myFunction` contains a few lines of code. Python uses indentation to show that the two print statements are a part of the function. Any time `myFunction()` is run, python will run all code contained within the function.

## Variables
```py
myInteger = 1234                        # An integer (whole number)
myFloat = 12.34                         # A float (decimal number)
myString = "This is also a variable"    # A string
myArrayOne = [1,2,3,4,5]                # An array containing integers
myArrayTwo = [True, True, False]        # An array containing booleans
myArrayThree = ["one", "two", "three"]  # An array containing strings
myBoolean = True                        # A Boolean
```
Variables allow us to store data and give it a name that can be referenced later. Some examples of data that can be stored include numbers, strings, boolean (True/False value), and arrays (lists of other values). 

## If statements
```py
# Example 1
if myBoolean == True:
    print("It's true!")
else:
    print("It's false!")

# Example 2
if myInteger == 0:
    print("It's zero!")
else if myInteger == 1:
    print("It's one!")
else:
    print("It's not one or zero!")
```
If statements are important for selecting different options based on condition. In example 1, the condition is a boolean 


## While Loops
```py
while myInteger < 10:
    print(myInteger)
    myInteger = myInteger + 1
```

## For loops
```py
# Example 1
for x in [1,2,3,4]:
    print(x)        # prints numbers from 1 to 4, one at a time.

# Example 2
for x in range(10):
    print(x)        # prints numbers from 0 to 9, one at a time.

# Example 3
for x in "string of text":
    print(x)

```
For loops are used to iterate over several pieces of information. Frequently, this takes the form of an array. In example 1, we are iterating over each piece of data in the array `[1,2,3,4]`. The statements inside the for loop will be run once for each iteration. The first time, "1" will be printed, the second time "2" will be printed, and so on.

In the second example, the `range()` function is used. This tells the for loop to iterate over a range of numbers from 0 to N (not inclusive).

The third example demonstrates iterating over letters in a string. The string is used as though it is an array of single letters. The first iteration will print "s", the second will print "t", and so on.

## Working with Files
```py
with open('test.txt', 'r') as file:     # open a file called "test.txt"
    for line in file:                   # iterate over each line in file
        print(line)
```
This example shows how to open a file using a `with` statement. The with statement simplifies this process by handling the opening and closing of the file. Contents of the file will be available to any code written in the indented block under the with statement, in this case, a for loop.

