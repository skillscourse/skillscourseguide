# <img width=25px height=30px style='margin-right: 10px; margin-bottom: -6px;' src="https://s3.dualstack.us-east-2.amazonaws.com/pythondotorg-assets/media/community/logos/python-logo-only.png"> Python Topical Unit
**Note: This module is not applicable if you have taken CS128 at Earlham**

Python is a widely-used, interpreted, object-oriented, high level programming language. It is very popular, particularly among domain scientists such as biologists. This language unit is designed for people that have not taken CS128 at Earlham. If you are in a major other than CS, or have previous programming experience in a language other than Python, this module might be perfect for you.

This course emphasizes learn-by-doing, so to learn Python you will write programs in Python. The way to learn a programming language is to read and write code in that language. 

## Resources (and additional READMEs)
- [Using Jupyterhub README](../../supplimental-material/JUPYTER.md)
- [Python Basics README](BASICS.md)
- [Codingbat Python Practice](https://codingbat.com/python)
- [W3Schools Python](https://www.w3schools.com/python/python_intro.asp)

## Learning Goals
- Gain a basic understanding of python assuming a very light programming background, or none at all.
- Understand and be able to use both decision making statements and loops.
- Understand the use and importance of comments in code.

## Getting Started
You can either install python on your own machine, or use one of the CS servers to write your code. You can access and use [Jupyterhub](https://jupyter.cs.earlham.edu) with CS credentials (send an email if you need credentials). If you want to install Python on your own machine, you can find a current version [here](https://www.python.org/downloads/). We recommend using [VScode](https://code.visualstudio.com/) as an editor on your machine.

## Deliverables
1. Write a helloWorld script in python. 
   - It should contain a single function called `helloWorld()` that prints the string "Hello World!".
2. Write a second script in python that does the following:
   - Read a file containing an arbitrary number of integers, one per line. Display the count, total, and average on stderr at the end of the run. Make the program capable of displaying the maximum values of each quantity without resorting to arbitrary precision arithmetic (hint, unsigned long int). 
   - Use a function/method as part of your program, e.g. to write the aggregated values to stdout. 
   - Use command line parameters available via argv and argc to support `-i input-file-name` to pass the input file to your program. 


## Notes / Ideas
- Using a notebook (ipynb)
  - jupyter.cs.earlham.edu
  - should prep for the data analysis module
  - lab 1-4 of 128
- Using python on the command line (py)
  - short introduction. Start by pasting code from a notebook into a .py file