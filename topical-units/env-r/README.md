# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://www.pngall.com/wp-content/uploads/2017/05/Copyright-Symbol-R-Free-Download-PNG.png"> R Topical Unit

R is a programming language widely used by domain scientists (i.e. biologists, physicists) to visualize datasets. The focus of this unit will be on the language, but it will also cover the R terminal console and RStudio, as well as the common R package ggplot. This unit will not cover deeper nuances of data science, statistics, and modeling, except incidentally.

# Resources
 - [Rstudio Web (rstudio.cluster.earlham.edu)](https://rstudio.cluster.earlham.edu/)
 - [Install R and R Studio](https://posit.co/download/rstudio-desktop/)
 - [Intro to ggplot2](https://towardsdatascience.com/r-for-beginners-learn-how-to-visualize-data-like-a-pro-840d1828c09c)
 - [Software Carpentry R Tutorial](https://swcarpentry.github.io/r-novice-gapminder/)

## Getting Started 
You have a few different options for using R:
1. The lab computers in Lovelace (CST 219).
2. Installing R Studio on your own person machine (Link in resources).
3. Use Rstudio web on ECCS servers (Link in resources).

Once you have an Rstudio session with any of these options, take some time to explore and learn about the interface. Test out some of the basic instructions from any of the resources above, and ask questions when you have them.

When you feel comfortable navigating, proceed to the deliverables below.

# Deliverables

You will build a series of data visualizations (almost) from scratch.

You may bring your own dataset, or you may start by looking at [some datasets](http://cluster.earlham.edu/datasets) we have curated. (If you bring your own and prove it's substantive, we will add it to that webpage.) If you need to store a big dataset (>10s of GBs), check with an instructor first.

You will use the `ggplot2` library. Additionally, you may discover value in the `vegan`, 

Here are the data analyses and visualizations we need to see by end of term:
* Load the data.
* Create four (different) R visualizations comparing a few categories within a dataset (could be a line graph, bar graph, box-and-whisker, or another visualization that fits the data).
* For each visualization, write a caption.
* Make your visualizations look good! (Really, this will be part of your evaluation.)
