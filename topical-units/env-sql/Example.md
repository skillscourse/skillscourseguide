# Example - working with SQL data
For this example, you will use the Icelandic Weather data that can be found in this repository. For your own work, you are welcome to use a different CSV of whatever data you find interesting. 


## 1. Create a table to load with data
**NOTE:** You should do steps 1-5 in in [Getting started](README.md#getting-started) before you do the following example.

The CSV we are importing from (`Iceland-Weather.csv`) does not have a "header row", which would normally serve as labels for the columns of data. This will make it easier to import, and we can use SQL to create columns with the labels we need.
```sql
create table iceland_stations(
station_id integer,
record_year integer,
record_month integer, 
mean_month_temp float,
avg_max_daily_temp float,
max_temp float, 
max_temp_record_day integer, -- date of highest maximum temperature
avg_min_daily_temp float, 
min_temp float, 
min_record_day integer, 
mean_rh float, 
precipitation_total float, 
max_precipitation float, 
max_precipitation_record_day integer, 
avg_bph float, 
mean_cloud_cover float,
sun float, 
mean_wind_speed float); 
```

## 2. Adding data from the CSV to the columns
Now we need to copy the data. You can download the CSV from this repository, or there is a copy available here:

`/eccs/home/pelibby16/CS266-SQL-Materials/Iceland-Weather.csv`

Run the following SQL command with the PATH_TO_DATA replaced correctly:
```sql
\copy iceland_stations from 'PATH_TO_DATA' with (format csv, delimiter ',', null 'NULL')
```

## 3. A few examples queries to get you thinking
Once you have your database set up, try running the following query to test that everything is working. You should get a graph (drawn with #'s) that represents some part of the data. Think about what each part of this statement is saying. Break it down into chunks, and research any works that don't make sense such as `trunc` or `lpad`.
```sql
select trunc(mean_month_temp) as mean, lpad('', count(*)::int, '#') as frequency from iceland_stations group by trunc(mean_month_temp) order by trunc(mean_month_temp);
```
```sql
select width_bucket(mean_month_temp, -4, 12, 25), count(*), min(mean_month_temp), max(mean_month_temp) from iceland_stations group by 1 order by 1; 
```
```sql
select width_bucket(mean_month_temp, -4, 12, 30), count(*), min(mean_month_temp), max(mean_month_temp), lpad('', count(*)::int, '#') as theBar from iceland_stations group by 1 order by 1; 
```

## 4. Using other languages to interact with SQL
The following code example makes use of the `psycopg2` library for python to interact with a PSQL database. 

```py
import psycopg2
import sys
import matplotlib.pyplot as plt

def main():
	date = []
	temperature = []
	connectionString = "host='localhost' dbname='charliep' user='charliep'"
 
	print("Connecting to database\n	->%s" % (connectionString))
	connection = psycopg2.connect(connectionString)
	print("Connected!\n")
 
	cursor = connection.cursor()
	sqlStatement = "\
		select record_year::float + (record_month::float/10), mean_month_temp\
		from iceland_stations\
		order by record_year, record_month"
	cursor.execute(sqlStatement)
	print("Selected!")
	
	rows = cursor.fetchall()
	print("Fetched!")
	
	if rows is not None:
		for x in range(0, cursor.rowcount):
			date.append(rows[x][0])
			temperature.append(rows[x][1])

# 	for x in range(0, cursor.rowcount):
# 		print(date[x], temperature[x])
		
	plt.plot(date[:], temperature[:])
	plt.xlabel("Year and Month")
	plt.ylabel("Mean Temperature C")
	plt.title("Monthly mean temperature at Dalatangi, Iceland (Station 620)")
	plt.show()	  

	cursor.close()
	connection.close()
	
if __name__ == "__main__":
	main()
```