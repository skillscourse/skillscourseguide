# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png"> SQL Topical Unit

SQL or "Structured Query Language" is the standard language for accessing relational databases. Most of the world's data is stored in relational databases, and SQL is the most commonly used language for working with data from an application written in Python, PHP, Javascript, C, Java, etc. This module uses the Postgres relational database management system as the technical basis. 


# Resources

We will provide you a personal database (`$username_db`) on the main CS server. We will also give you access to pre-existing data collections, see below.
- [Our Example](Example.md)
- [w3schools SQL (with interactive demos)](https://www.w3schools.com/sql/)
- [SQL Basics Cheatsheet](https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-letter.pdf)
- Some data sets you can use:
  - [Icelandic Weather data](Iceland-Weather.csv) (In this repository)
  - [Richmond Indiana percipitation data](richmond-indiana-water-works.csv) (In this repository)
  - [data.gov datasets](https://catalog.data.gov/dataset/?res_format=CSV)
 

# Learning Goals

After completing this unit, you will be able to work directly with SQL databases and perform better queries using SQL in code for e.g. websites and applications.

* DDL - create table, alter table, drop table, create index 
* DML - select, insert, update, delete 
* DCL - grant, revoke 
* Importing data from data files 
* psql commands such as `\h`, `\e`, and `\r`.
* Embedding SQL in Python with psycopg and running 
* Installing Postgres client libraries on your own machine (optional)

# Getting Started 

1. Request a Postgres database from the CS system administrators, a polite email to admin@cs.earlham.edu is usually sufficient. Explain that you are working on the skills course SQL unit. This will give you a sandbox to work in. 

2. Test your ability to ssh to bowie.cs.earlham.edu, this is the machine you will first use to access your Postgres database. `$ ssh username@bowie.cs.earlham.edu` If you haven't already setup your ssh keys now would be a great time to do so. 

3. Test your ability to connect to the database via psql from the command line on bowie. `$ psql -d your-database-name` 

4. List all the database objects you have access to. `your-database-name=# \d`

5. Create a table, insert some data, view it, and then drop the table:
   - `create table test (column1 int);`
   - `insert into test values (1);`
   - `select * from test;`
   - `drop table test;`

6. Proceed to work on the deliverables. Check out the [Example](Example.md) if you need help getting a full-sized dataset imported into SQL.

# Deliverables

You will deliver a file of SQL queries. Some code you must include:
* several `SELECT` statements
* a query using `GROUP BY`
* a query using `ORDER BY`
* a query using `LIMIT`

For each query, include:
* the query itself
* the output (or an excerpt of the output if there are many many rows)
* a single-sentence comment about why you ran that particular query

You may also submit Python, JavaScript, or other external/interactive code that uses some SQL calls and renders the data (in text or visuals). The [weather data example](Example.md)  takes you from CSV -> RDBMS -> Python/MatPlotLib -> graphs of data. 