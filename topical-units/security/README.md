# Cybersecurity Topical Unit

This unit will introduce tools for thinking about cybersecurity or, more appropriately today, just *security*. These will include concepts like social engineering, ethical hacking, and security policy best practices. You will learn some of the common vulnerabilities in a system, get started with the fantastic networking tool tcpdump, and explore security measures on a system using the Earlham CS environment as an example.

The first thing to do is read this [overview of cybersecurity from IBM](https://www.ibm.com/topics/cybersecurity).

# Resources

* [Electronic Frontier Foundation's extensive security guide](https://ssd.eff.org/) or shorter guides like [this one](https://ssd.eff.org/en/module/seven-steps-digital-security)
* ["There is no longer any such thing as computer security"](https://blog.codinghorror.com/there-is-no-longer-any-such-thing-as-computer-security/) - pay more attention to concepts than to e.g. "iPhone vs. Android" - the specifics vary widely
* [DevOps Security Tools Worth Using](https://nullsweep.com/devops-security-tools/)
* [Bruce Schneier](https://www.schneier.com/)

# Tools
* [tcpdump](https://www.tcpdump.org/)
* nslookup and/or dig, ip and/or ifconfig
* whois, traceroute, ping 
* Optional - nmap, public key cryptography, block chain

Using these tools to learn about any given IP number is an iterative process. `whois` will tell you about who owns the domain, nameserver IPs, etc. It often works differently on the IP number for a host than the DNS name. 

`traceroute` will show you the route between the host you run it on and the destination IP address.

# Learning Goals

You will become conversant in both technical and social aspects of security, at both a macro level (hacking, best practices) and a micro level (keeping your own information as safe as you reasonably can).

This unit is in many respects more conceptual than the other topical units. For this reason, we'll be using more reading and writing than in some others.

# Deliverables

There are exercises and a writeup to go with this topical unit.

## Exercises
* On bowie (the CS server), use tcpdump to monitor ping traffic. Then, also using tcpdump, check web traffic.
    * You will need permission to run this command. Reach out to Charlie and Porter to get it.
* Tell us everything you can about a given IP address (we will provide this):
    * route to it
    * DNS name
    * DNS server
    * Who owns it? What is the registered street address?
* tcpdump

## Writing
A big part of introducing security is vocabulary (more so than in many other units). Security is a broad topic, and these terms will help frame your thinking without abstracting too many key details away.

* Provide a writeup (1-2 sentences each) of the following concepts, with one example each:
    * social engineering
    * ethical hacking
    * firewall
    * phishing
    * ransomware
    * encryption
    * two-factor authentication
    * buffer overflow
    * integer overflow
    * code injection vulnerabilities
    * race condition vulnerabilities
    * privilege confusion
    * password manager (maybe begin using one...)
* Answers to the following questions:
    * What's a VPN and what are the tradeoffs associated with using versus not using a VPN? (Hint: the answer is not "a VPN is automatically more secure".)

Written answers should be in the form of a well-organized, correctly-formatted Markdown file.
