# System Administration Topical Unit
This unit introduces the most common skills for systems and network administration. This includes commands used to access system configuration information, accessing log and configuration files, tracing the activity of a process, and troubleshoot common user and system issues. There is a heavy emphasis on command-line utilities, including text editors within the terminal. This material also includes cloud technologies like containers and virtual machines which have both become integral to deploying compute resources.

# Learning Goals

* Learn how to setup a Debian Linux virtual environment with Virtualbox or a similar Hypervisor (Check out UTM if you are using an M1/M2 Macbook)
* Learn how to determine the IP and network route information for a host. 
* Learn how to determine the process, memory, and disk load for a host. 
* Learn how to start, stop, and inspect system services. 
* Learn about the /proc filesystem. 


# Resources

If you haven't been working on the terminal for a while, the CS fundamentals unit has a [page about Linux command line utilities](../../cs-fundamentals/command-line.md) that you should review. Run some commands to refresh your memory.

* [Software Carpentry's Unix walkthrough](https://swcarpentry.github.io/shell-novice/)
* [The /proc Filesystem Documentation](https://tldp.org/LDP/sag/html/proc-fs.html)
* [DigitalOcean Guide to systemctl](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)
* Downloads
  * [Virtualbox (Windows, Linux, Intel Macbooks)](https://www.virtualbox.org/wiki/Downloads) or [UTM (M1/M2 Macbooks)](https://mac.getutm.app/)
  * [Debian 12 download page](https://www.debian.org/download)

# Notes
* Make sure to uncheck "Desktop Environment" while installing Debian. We want a command-line only installation. If you end up with a Desktop GUI, something has been configured incorrectly.
* After installing, you may still get the "Debian Installer" when you boot the VM. This could be due to the ISO disk image still being started, instead of the operating system you just installed. Check in your VM settings for where the disk image is references, and "clear" or "unmount" it.
* 10-12GB should be plenty of space for your virtual machine. You can delete this after the course, all the space will be recovered.
* 2GB of ram is plenty for a virtual machine.
* Make sure to write down the passwords you use for your account and for root.

# Deliverables
* Write a script (or set of scripts) that displays the following information about your virtual machine in a clear and readable way:
  * The configured IP devices and routes. 
  * A summary of the machine's load over a 5 minute period. 
  * All the system services that have failed since the last reboot. 
  * Show all the configured block devices on a machine (i.e. disk drives) using the `/proc` filesystem