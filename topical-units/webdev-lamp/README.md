# WebDev Topical Unit

This unit will introduce you to fundamental skills for website development. These will include the “LAMP” stack - Linux, Apache, MySQL, PHP - as well as HTML, JavaScript, and CSS. You will build a simple website as your main project, and in doing so you may begin building and sharing a project portfolio.

# Learning Goals

In this unit you will learn:

* HTML
* CSS
* JavaScript
* the LAMP stack
* the developer console in your web browser

The webpages you build for this course will be hosted by Earlham CS but you may copy the files to be hosted anywhere you'd like if you so choose.

# Resources

## Initial setup

First, ssh to bowie. If it doesn't already exist, create the directory `www`. Inside create a file called `index.html` with the text "Hello, World!". To confirm it's ready, go to `cs.earlham.edu/~$username` in your favorite web browser.

You're all set to work on this unit.

## Interactive and Video-based Materials

The W3 School's material is very good, start with this [tutorial](https://www.w3schools.com/html/default.asp) working through the JavaScript unit.

TutorialsPoint has videos on YouTube, [this one](https://www.youtube.com/watch?v=NAEHbzXMNpA) is a good place to start.

# Deliverables

## Project: Build a personal portfolio website

For this unit, you will build a professional personal website from scratch using a combination of the tools above.

Your website will include:
* a home page saying something about you
* a blog page with multiple posts
* styling with CSS
* buttons to facilitate interactions on the page

You're not required to use this forever and for always. Also, if you don't wish to post personal information online, you are free to propose an alternative website to develop the same skills.

You are posting on a college website for review by your faculty, so we encourage creativity, humor, and uniqueness. You are not graded for content, but it's the place where you can have the most fun.

