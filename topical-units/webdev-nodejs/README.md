# <img width=30px height=30px style='margin-right: 10px; margin-bottom: -3px;' src="https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png"> Webdev 2 - NodeJS Topical Unit
This unit will introduce you to fundamental skills for NodeJS and NPM. These tools are the basis for a lot of modern frameworks and webstacks, as well as Discord bots, APIs, and other server-side services. 

Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. It allows developers to run JavaScript on the server side, creating server-side applications with JavaScript. Node.js also includes a large and active ecosystem of open-source libraries that can be used to build a variety of applications. It is especially useful for building real-time, high-concurrency, and high-performance applications, such as web servers, chat servers, and online games.

This unit assumes that you have some knowledge of HTML, CSS, and Javascript, as well as understanding the fundamentals of Git. If you would like to learn more, we have units for those:

[Learn about Git](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/blob/master/cs-fundamentals/git.md)

[Learn about HTML/CSS/JS](https://code.cs.earlham.edu/skillscourse/skillscourseguide/-/tree/master/webdev)

# Learning Goals
- Understand the basics of Node.js, including how to install it and run simple JavaScript scripts.
- Learn how to use the Node.js runtime to execute JavaScript code on the server side.
- Understand the basic structure of a Node.js application, including how to use modules to organize code and how to use the require function to include modules in your application.
- Understand how to use Node.js to read and write files, and how to serve static files to clients.
- Understand how to use Node.js to handle HTTP requests and responses, and how to send and receive data over the network.
- Learn how to use Node.js to create a simple command-line application.
- Learn how to use a router module, such as Express.

Overall, the goal of this project is to get a basic understanding of how Node.js works and how to use it to build simple applications.

# Terminology
**Web Server** - A web server is a software program that listens for requests from clients, and sends back responses over the internet. The most common type of web server is a web server that serves content over the HTTP (Hypertext Transfer Protocol) or HTTPS (HTTP Secure) protocols.

**Routes** - In a Node.js application, routes are the paths that the application can handle requests for. For example, in a web server built with Node.js, a route might be a path like "/about" or "/contact", which the server can handle by returning the appropriate content to the client.

**Router** - In the context of Node.js, a router refers to an object that is responsible for handling HTTP requests and mapping them to a specific action. It is often used in web applications to direct incoming requests to the appropriate handler function based on the requested URL and HTTP method.

**Package Manager** - a package manager is a tool that helps you manage the dependencies (libraries and frameworks) that your application relies on. It allows you to easily install, update, and manage the packages that your application needs to function properly.



# Resources
- [Sample Project Steps README](SAMPLE_PROJECT.md) - A guide made specifically to help form an understanding of the basics.
- [NodeJS Guides](https://nodejs.org/en/docs/guides/) - Official guides from NodeJS, lots of information and details about what is possible.
- [Express Routing Guide](https://expressjs.com/en/guide/routing.html) - Guide from Express, lots of information about what can be done with routes and how.
- [NPM Docs](https://docs.npmjs.com/about-npm) - NPM package manager docs, and information about using and creating packages.
- [NodeJS Youtube Guide](https://www.youtube.com/playlist?list=PL55RiY5tL51oGJorjEgl6NVeDbx_fO5jR) - A good series of short videos, uses similar technologies to our own guide, but goes further.

# Deliverables
Create a simple NodeJS server that serves an HTML page at the default route, and present some set of data on it (this can be anything). Store your data on the server side, and use a route to pass it to the client. Use another page on a different route to allow users to manipulate this data in some way that can be seen on the 'home' page.
Take a look at the [Sample Project](SAMPLE_PROJECT.md) if you get stuck.

Bonus: Try to store the data in a way that is persistent, so that when the NodeJS server is restarted, modifications and additions are kept.
