# Sample NodeJS Project
The NodeJS ecosystem is vast and intricate. It can be tricky to know how and where to get started. The following is included to guide you through the "Hello World" equivalent, and give you the tools you need to build your own projects.

# Getting Started
0. Follow the instructions [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) to install NodeJS and npm on your system.

1. Create a folder or git repository for your project.

2. From a terminal within that folder, run `npm init`. You will be prompted to enter various bits of metadata about your project. These can be easily customized later, so spend too much time on it for now.

3. Take a look at the `package.json` file that is generated in your project, this file holds all the information for Node to use. It should appear something like this:
    ```
    {
        "name": "nodejs_tutorial_project",
        "version": "1.0.0",
        "description": "project to explain creation of a nodejs project",
        "main": "index.js",
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        "author": "Porter Libby",
        "license": "ISC"
    }
    ```

4. Since the `package.json` "main" directive points to a file called `index.js`, we need to create that file. This is the server-side component of NodeJS that will run backend tasks and serve clients.

5. Install the Express npm package with `npm install express`, notice that this generates a `node_modules/` folder. This folder contains all the code and libraries that NodeJS will use in your project. You should never need to edit these dependency files, and you should use a `.gitignore` file to avoid uploading them to your repository. Any fresh clones will be able to create an identical `node_modules/` directory just by running the `npm install` command, since your `package.json` keeps track of what packages you have installed in your project (Can be found under "dependencies").

6. Now that we have things set up, we can start writing our server-side script. In your `index.js`, try out the following Hello World example:
    ```
    var express = require('express'); // include express dependency
    var app = express(); // create an express app

    // set up default route
    app.get('/', function (req, res) { 
        res.send('Hello World');
    });

    // start server
    var server = app.listen(8081, function () { 
        var host = server.address().address
        var port = server.address().port
        
        console.log("Example app listening at http://localhost%s:%s", host, port)
    });
    ```
    This is a basic server which sends the string "Hello World" to clients. If you open `localhost:8081` in a browser, you should see the string printed to the screen. 

7. Next, lets update this to serve an HTML file instead of just a string. First, create a new folder in your project called `public`. This folder will store all of the client-side files. Inside public, create an html file called `index.html`. This will be our "home page". You can include any HTML you like on this page. This folder structure is just an example, but the given names are common and used widely. Look at the bottom of this readme for a diagram if you get confused. 

8. Now we need to modify our server-side file (`index.js`) to serve this HTML file, instead of the "Hello World" string. In our default route, we want to replace `res.send('Hello World');` with:
    ```
    res.sendFile(__dirname + '/public/index.html');
    ```
    Now, instead of sending a string to connecting clients, the server will send the HTML page to clients.

9. What we have now will work fine for a single HTML page, but most websites use more than one file in order to organize styles (CSS) and scripts (client-side JavaScript). In order to do this with NodeJS, the server needs to know what extra files to send each client. For this, add a `use` directive to the `index.js`, right after the default route:
    ```
    app.use(express.static(__dirname + '/public'));
    ```

    This will tell the server to include the folder `public/` with all its contents and subdirectories in the information it sends to each connected client. Now we can create css and js files in the `public/` folder and link them to our `index.html`.
# More options with Routes
Lots of things can be accomplished with what you have learned so far. Lets talk a bit more about different ways you can use routes, with some examples of code you could add to your `index.js`.

## Adding a second HTML route
```
// in index.js

app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/public/about.html');
});
```

Opening `localhost:8081/about` will access this route, displaying the `about.html` page instead of our default `index.html`. 

## Adding a route to a server-side function
On the server-side, in `index.js` (or equivalent), add a route that returns a value from a function:
```
app.get('/my-function', myFunction);

function myFunction(req,res){
    // do something on the server-side
    // such as read a file or calculate a value
    console.log(req.query.myVar); // console log the value passed from the client
    res.send("myValue");
}
```
On the client-side, in `/public/js/main.js` (or equivalent), use Ajax or similar to send a request to the server at the route you just created on the server-side:
```
// in /public/js/main.js
$.ajax({
        type: 'GET',
        url: '/my-function',
        data: {myVar: value},
        success: function(response) { 
            console.log(response) // value from the server
        },
        error: function(xhr, status, err) {
            console.log("Error");
        }
    });
```
This allows you to hand values, strings, or objects from the server-side to the client-side. This is an extremely useful feature, as it allows us to hide certain information from the client, such as API keys or other credentials by only showing the client the result, and not the process.

# Layout
Here is a brief map of what a typical NodeJS project might look like:
- **`index.js`** - Server-side Javascript, runs on the host machine.
- **`package.json`** - NodeJS metadata.
- *node_modules/* - Modules installed through NodeJS.
- *public/* - Folder of all client-side materials.
    - **`index.html`**
    - *css/*
        - **`main.css`**
    - *js/*
        - **`main.js`**
